# Final Project Back-end

## Features

- GET, POST, PUT, DELETE posts
- GET, POST, PUT, DELETE comments
- GET, POST, PUT, DELETE users
- Swagger for visualizing and interacting with the API’s resources without having any of the implementation logic in place

## Using

- Spring/Java
- H2 Database
- Swagger UI

## Installation and Usage

- Step 1: Clone or download the source code by using command:

```batch
https://gitlab.com/hnminh/final_assignment
```

- Step 2: Change your java version in line 17 of `src/main/resources/application.properties` file

- Step 3: Install all of its dependencies and package:

```
mvn install
```

- Step 4: Run on your own machine and open port 8080:

```
java -jar target/finalAssignment-0.0.1-SNAPSHOT.jar
```

- Step 5: Access to the Swagger UI with the username: <b>user</b> and password: <b>password123</b> on the url [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

## Deployment

+ We deployed to Heroku and here is the link of our API:
[https://e1800950-final.herokuapp.com](https://e1800950-final.herokuapp.com)

+ We can also access to Swagger UI with the same username and password above through the link:
[https://e1800950-final.herokuapp.com/swagger-ui.html](https://e1800950-final.herokuapp.com/swagger-ui.html)
