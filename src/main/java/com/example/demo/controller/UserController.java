package com.example.demo.controller;

import java.util.Optional;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class UserController {
    @Autowired
    private UserRepository repository;

    @GetMapping("/users")
    public Iterable<User> list(){
        return repository.findAll();
    }

    @GetMapping("/user/{id}")
    public Optional<User> get(@PathVariable("id") int id){
        return repository.findById(id);
    }

    @PostMapping("/user")
    public @ResponseBody User create(@RequestBody User item){
        return repository.save(item);
    }

    @PutMapping("/user")
    public @ResponseBody User update(@RequestBody User item){
        return repository.save(item);
    }

    @DeleteMapping("/user")
    public void delete(@RequestBody User item){
        repository.delete(item);
    }
}