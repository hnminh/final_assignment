package com.example.demo.controller;

import java.util.Optional;

import com.example.demo.model.Post;
import com.example.demo.repository.PostRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    private PostRepository repository;

    @GetMapping("/posts")
    public Iterable<Post> list(){
        return repository.findAll();
    }

    @GetMapping("/post/{id}")
    public Optional<Post> get(@PathVariable("id") int id){
        return repository.findById(id);
    }

    @PostMapping("/post")
    public @ResponseBody Post create(@RequestBody Post item){
        return repository.save(item);
    }

    @PutMapping("/post")
    public @ResponseBody Post update(@RequestBody Post item){
        return repository.save(item);
    }

    @DeleteMapping("/post")
    public void delete(@RequestBody Post item){
        repository.delete(item);
    }
}