package com.example.demo;

import com.example.demo.model.Comment;
import com.example.demo.model.Post;
import com.example.demo.model.User;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.PostRepository;
import com.example.demo.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FinalAssignmentApplication {

	@Autowired
	UserRepository userRepository;
	@Autowired
	PostRepository postRepository;
	@Autowired
	CommentRepository commentRepository;
	public static void main(String[] args) {
		SpringApplication.run(FinalAssignmentApplication.class, args);
	}

	@Bean
	public void initData(){
		User user1 = new User("Minh");
		User user2 = new User("Hoang");
		userRepository.save(user1);
		userRepository.save(user2);

		Post post1 = new Post(1, "Hello everyone!");
		Post post2 = new Post(1, "I am new to this");
		Post post3 = new Post(2, "Hello world!");
		postRepository.save(post1);
		postRepository.save(post2);
		postRepository.save(post3);

		Comment comment1 = new Comment(1, 2, "Hello Minh!");
		Comment comment2 = new Comment(2, 1, "Ciao!");
		Comment comment3 = new Comment(1, 1, "Hello Hoang!");
		commentRepository.save(comment1);
		commentRepository.save(comment2);
		commentRepository.save(comment3);
	}

}
