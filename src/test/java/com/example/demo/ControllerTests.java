package com.example.demo;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import com.example.demo.model.Comment;
import com.example.demo.model.Post;
import com.example.demo.model.User;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.PostRepository;
import com.example.demo.repository.UserRepository;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.security.test.context.support.WithMockUser;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTests extends AbstractTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;

    @WithMockUser("USER")
    @Test
    public void getUserList() throws Exception {
        // Lets create an item, the database will retun the object
        // with given id and the two object made by Bean in
        User user = new User("Ngoc");
        user = userRepository.save(user);
        // the database returns the object inside an array, let create similar
        User[] arr = new User[1];
        arr[0] = user;
        MvcResult mvcResult = mvc.perform(
                MockMvcRequestBuilders.get("/user/" + user.getId()).accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(super.mapToJson(user), content);
    }

    @WithMockUser("USER")
    @Test
    public void createUserViaPost() throws Exception {
        User user = new User("ABCDE");
        String inputJson = super.mapToJson(user);
        // The Bean already inserted two objects and previous test third one
        // so expecting to get id = 4
        user.setId(3);
        System.out.println(inputJson);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/user")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(super.mapToJson(user), content);
    }

    @Test
    public void postGetDeleteComment(){
        Iterable<Comment> begin = commentRepository.findAll();
        Comment comment = new Comment(4, 1, "ABCD");
        Comment saved = commentRepository.save(comment);
        Optional<Comment> found = commentRepository.findById(comment.getId());
        assertEquals(found.get().getId(), saved.getId());
        Iterable<Comment> end = commentRepository.findAll();
        assertEquals((long) IterableUtils.size(begin) + 1, (long) IterableUtils.size(end));
        commentRepository.deleteById(comment.getId());
    }

    @Test
    public void postGetDeletePost(){
        Iterable<Post> begin = postRepository.findAll();
        Post post = new Post(1, "ABCD");
        Post saved = postRepository.save(post);
        Optional<Post> found = postRepository.findById(post.getId());
        assertEquals(found.get().getId(), saved.getId());
        Iterable<Post> end = postRepository.findAll();
        assertEquals((long) IterableUtils.size(begin) + 1, (long) IterableUtils.size(end));
        postRepository.deleteById(post.getId());
    }
}
